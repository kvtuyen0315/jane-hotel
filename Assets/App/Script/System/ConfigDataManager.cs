using AFramework;
using System;
using System.Collections;
using UnityEngine;

public class ConfigDataManager : SingletonMono<ConfigDataManager>
{
    public static ConfigMap configMap { get; private set; }
    public static ConfigStory configStory { get; private set; }
    public static ConfigCharacter configCharacter { get; private set; }
    public static ConfigItem configItem { get; private set; }
    public static ConfigNpcAI configNpcAI { get; private set; }

    public bool isDoneInit { get; set; }

    public void Init(Action callback)
    {
        StartCoroutine(ProgressLoadConfig(callback));
    }

    private IEnumerator ProgressLoadConfig(Action callback)
    {
        if (!isDoneInit)
        {
            configMap = Resources.Load<ConfigMap>("ConfigData/ConfigMap");
            while (!configMap)
            {
                yield return null;
            }

            configStory = Resources.Load<ConfigStory>("ConfigData/ConfigStory");
            while (!configStory)
            {
                yield return null;
            }

            configCharacter = Resources.Load<ConfigCharacter>("ConfigData/ConfigCharacter");
            while (!configCharacter)
            {
                yield return null;
            }

            configItem = Resources.Load<ConfigItem>("ConfigData/ConfigItem");
            while (!configItem)
            {
                yield return null;
            }

            configNpcAI = Resources.Load<ConfigNpcAI>("ConfigData/ConfigNpcAI");
            while (!configNpcAI)
            {
                yield return null;
            }

            foreach (ConfigMapRecord configMapRecord in configMap.records)
            {
                MapInfo mapInfo = Resources.Load<MapInfo>("Map/" + configMapRecord.linkMapPrefab);
                GameLoadData.dicMapInfo.Add(configMapRecord.mapName, mapInfo);
            }

            foreach (ConfigCharacterRecord configCharacterRecord in configCharacter.records)
            {
                BaseCharacter character = Resources.Load<BaseCharacter>("Character/" + configCharacterRecord.linkPrefab);
                GameLoadData.dicCharacter.Add(configCharacterRecord.name, character);
            }

            foreach (ConfigItemRecord configItemRecord in configItem.records)
            {
                BaseItem item = Resources.Load<BaseItem>("Item/" + configItemRecord.linkPrefab);
                GameLoadData.dicItem.Add(configItemRecord.name, item);
            }

            var arraySpriteEmoji = Resources.LoadAll<Sprite>("ImageData/Emoji");
            for (int i = 0; i < arraySpriteEmoji.Length; i++)
            {
                GameLoadData.dicSpriteEmoji.Add(arraySpriteEmoji[i].name, arraySpriteEmoji[i]);
            }

        }

        yield return null;

        callback?.Invoke();
    }

}
