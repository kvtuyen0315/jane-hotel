﻿using AFramework.IAP;
using System.Collections.Generic;
using UnityEngine;

public class GameIAPManager : AFramework.SingletonMono<GameIAP> {}

public class GameIAP : IAPManager, AFramework.ISaveData
{
    public const string FIRSTTIMEBONUS_Pack = "firsttimebonuspack";
    public const string UNIVERSE_Pack = "universalpack";
    public const string STARTER_Pack = "starterpack";
    public const string COMIC_Pack = "comicpack";
    public const string COMMON_Pack = "commonpack";
    public const string MONTER_Pack = "monterpack";
    public const string WEAPON_Pack = "weaponpack";
    public const string RemoveAdsPack = "removeads";

    public const string Reward_Universal = "universalpack";
    public const string Reward_Part = "part";
    public const string Reward_Character = "character";
    public const string Reward_Gold = "gold";
    public const string Reward_Key = "key";
    public const string Reward_DoubleGold = "doublegold";
    public const string Reward_BattlePass = "battlepass";

    public const string FirstPurchasePack = "1st";

    private bool DoubleCurrency { get; set; }

    public Dictionary<int, ActivePackageInfo> SingleCharPacks { get; protected set; }

    public ActivePackageInfo GetSingleCharPack(int id)
    {
        return SingleCharPacks.ContainsKey(id) ? SingleCharPacks[id] : null;
    }

    public Dictionary<int, ActivePackageInfo> OfferSingleCharPacks { get; protected set; }

    public ActivePackageInfo GetOfferSingleCharPack(int id)
    {
        return OfferSingleCharPacks.ContainsKey(id) ? OfferSingleCharPacks[id] : null;
    }

    public Dictionary<string, bool> IAPPackOwnedList = new Dictionary<string, bool>();
    public bool IsPurchasing { get; protected set; }

    protected Dictionary<string, int> _PackPercentOff = null;
    protected Dictionary<string, string> _PackSaleContent = null;

    protected override void Awake()
    {
        base.Awake();
        StartCoroutine(AFramework.Utility.CRDelayFunction(3, UpdateOwnedPack));
    }

    public override void PurchaseItem(string packageName, CallbackDelegate callback, string purchaseLocation)
    {
        IsPurchasing = true;
        CallbackDelegate warrper = (b1, s1) => {
            callback?.Invoke(b1, s1);
            IsPurchasing = false;
        };
        base.PurchaseItem(packageName, warrper, purchaseLocation);
    }

    protected override void TrackPurchase(string packageId, string location)
    {
        if (!mPackageIdToGroupPackageNameMapping.ContainsKey(packageId)) return;
        bool owned = IAPPackOwnedList[mPackageIdToGroupPackageNameMapping[packageId]];
        if (!owned)
        {
            base.TrackPurchase(packageId, location);
        }
    }

    protected override bool CanRestoreReward(string rewardName)
    {
        switch (rewardName)
        {
            case Reward_Universal:
            case Reward_Part:
            case RemoveAdsPack:
                return true;
        }

        return false;
    }

    public int GetPackPercentOff(string packName)
    {
        if (_PackPercentOff == null)
        {
            _PackPercentOff = new Dictionary<string, int>();
            //foreach (var item in ActivePackages)
            //{
            //    PackageInfoCustom packageInfoCustom = item.Value.CurrentActivePackage as PackageInfoCustom;
            //    if (packageInfoCustom.percentOff > 0)
            //    {
            //        _PackPercentOff[packageInfoCustom.PackageName] = packageInfoCustom.percentOff;
            //    }
            //}

        }

        if (_PackPercentOff.ContainsKey(packName))
        {
            return _PackPercentOff[packName];
        }
        return 0;
    }

    public string GetPackSaleContentOff(string packName)
    {
        if (_PackSaleContent == null)
        {
            _PackSaleContent = new Dictionary<string, string>();
            //foreach (var item in ActivePackages)
            //{
            //    PackageInfoCustom packageInfoCustom = item.Value.CurrentActivePackage as PackageInfoCustom;
            //    if (packageInfoCustom.percentOff > 0)
            //    {
            //        _PackSaleContent[packageInfoCustom.PackageName] = "Sale";
            //    }
            //}
        }

        if (_PackSaleContent.ContainsKey(packName))
        {
            return _PackSaleContent[packName];
        }

        return string.Empty;
    }

    public float GetFakePrice(string packName)
    {
        float offPercent = GetPackPercentOff(packName);
        return (int) (ActivePackages[packName].CurrentActivePackage.Price * 100 * 100 / (100 - offPercent)) / 100f;
    }

    public bool IsDoubleGold()
    {
        return mSaveData.DoubleGold;
    }

    public bool FirstPurchaseBonus()
    {
        return mSaveData.FirstPurchaseBonus;
    }

    public void ExternalPurchaseDoubleGold()
    {
        mSaveData.DoubleGold = true;
        DataChanged = true;
    }

    public void ExternalPurchaseBattlePass(bool from_ads = false)
    {
        if (!mSaveData.BattlePassOffer && (from_ads || CanTrackIAP()))
        {
            //GameTracking.TrackBuyBattlepass();
        }

        mSaveData.BattlePassOffer = true;
        DataChanged = true;
    }

    private bool IsRewardClaimed(string rewardName, int rewardValue)
    {
        return false;
    }

    protected override void OnPurchaseSucceededEvent(string packageId)
    {
        var pack = PackageIdentifierToActivePackage(packageId);
        if (pack == null) return;
        if (!(mPurchaseLocation == "restore" || mPurchaseLocation == "launch"))
        {
            //GameTrackingSingleton.I.TrackIAPProgress(packageId, mSaveData.PurchaseTime, GameData.I.GetStoryModeProgress(), GameData.I.GetSoftCurrency(), GameData.I.GetHardCurrency());
        }

        var originPackName = mPackageIdToGroupPackageNameMapping[packageId];
        bool owned = IAPPackOwnedList[originPackName];
        if (!owned)
        {
            ++mSaveData.PurchaseTime;
        }

        DoubleCurrency =
            (pack.CurrentActivePackage.PackageName.Contains("diamondpack") ||
             pack.CurrentActivePackage.PackageName.Contains("goldpack"));
        //    && GameData.I.GetPurchaseTime(originPackName) <= 0;
        //GameData.I.PurchasePackage(originPackName);
        DataChanged = true;
        base.OnPurchaseSucceededEvent(packageId);
    }

    protected override void HandleReward(string rewardName, int rewardAmount)
    {
        
    }

    protected override void OnIAPPackRefreshed()
    {
        UpdateOwnedPack();
        base.OnIAPPackRefreshed();
    }

    public bool IsPackOwned(string packname)
    {
        if (!ActivePackages.ContainsKey(packname)) return false;
        var packinfo = ActivePackages[packname].CurrentActivePackage;
        bool is_currency_pack = true;
        for (int i = 0; i < packinfo.Rewards.Count; ++i)
        {
            var reward = packinfo.Rewards[i];
            if (reward.Name == Reward_Gold)
            {

            }
            else if (reward.Name == RemoveAdsPack)
            {
                if (!GameAds.I.IsAdsRemoved()) return false;
                is_currency_pack = false;
            }
            else
            {
                Debug.LogError("TODO");
            }
        }
        return !is_currency_pack;
    }

    public void UpdateOwnedPack()
    {
        lock (mCurrentPackages)
        {
            foreach (var pair in mActivePackages)
            {
                if (pair.Value.CurrentActivePackage.PackageName == FIRSTTIMEBONUS_Pack)
                {
                    //IAPPackOwnedList[pair.Key] = !GameData.I.isFirstTimePack;
                    continue;
                }

                IAPPackOwnedList[pair.Key] = false;

                if (pair.Value.CurrentActivePackage.Type == AFramework.IAP.eProductType.NonConsumable)
                {
                    bool hasAllRewards = true;
                    for (int i = 0; i < pair.Value.CurrentActivePackage.Rewards.Count && hasAllRewards; ++i)
                    {
                        string strName = pair.Value.CurrentActivePackage.Rewards[i].Name;
                        if (strName == Reward_Gold) continue;

                        int amount = pair.Value.CurrentActivePackage.Rewards[i].Amount;

                        hasAllRewards = IsRewardClaimed(strName, amount);
                    }

                    IAPPackOwnedList[pair.Key] = hasAllRewards;
                }
            }
        }
    }

    public bool BattlePassOffer
    {
        get { return mSaveData.BattlePassOffer; }
    }

    [System.Serializable]
    public class SaveData
    {
        public bool FirstPurchaseBonus = true;
        public bool DoubleGold = false;
        public bool RemoveAdsOffer = false;
        public bool BattlePassOffer = false;

        public int PurchaseTime = 0;
    }

    protected SaveData mSaveData;

    public bool DataChanged { get; set; }

    public object GetData()
    {
        return mSaveData;
    }

    public void RegisterSaveData()
    {
        AFramework.SaveGameManager.instance.RegisterMandatoryData("gameiap", this as AFramework.ISaveData);
    }

    public virtual void SetData(string data)
    {
        if (string.IsNullOrEmpty(data))
        {
            mSaveData = new SaveData();
        }
        else
        {
            mSaveData = JsonUtility.FromJson<SaveData>(data);
        }
    }

    public void OnAllDataLoaded()
    {
    }
}