using AFramework;
using System.Collections.Generic;
using UnityEngine;

public class GlobalInfo : SingletonMono<GlobalInfo>
{
    #region Name menu & popup.
    public const string UIDefaultPath = "UI/";

    // Menu.
    public const string UIMainMenu = "Menu/MainMenu";
    public const string UILevelMapMenu = "Menu/LevelMapMenu";

    // Popup.
    public const string UIFlashScreenPopup = "Popup/FlashScreenPopup";
    public const string UIBlockPopup = "Popup/BlockPopup";

    #endregion
    public static ConfigStoryRecord currentStory { get; set; }

#if UNITY_EDITOR
    public static List<string> GetStringToAddOnList(string content)
    {
        List<string> list = new List<string>();
        var arrayStr = content.Split('\n');
        foreach (var item in arrayStr)
        {
            list.Add(item);
        }
        return list;
    }

    public static List<float> GetFloatToAddOnList(string content)
    {
        List<float> list = new List<float>();
        var arrayStr = content.Split('\n');
        foreach (var item in arrayStr)
        {
            list.Add(float.Parse(item));
        }
        return list;
    }

    public static List<int> GetIntToAddOnList(string content)
    {
        List<int> list = new List<int>();
        var arrayStr = content.Split('\n');
        foreach (var item in arrayStr)
        {
            list.Add(int.Parse(item));
        }
        return list;
    }

    public static List<eItemType> GetItemTypeToAddOnList(string content)
    {
        List<eItemType> list = new List<eItemType>();
        var arrayStr = content.Split('\n');
        foreach (var item in arrayStr)
        {
            eItemType itemType = eItemType.None;
            switch (item)
            {
                case "Key": itemType = eItemType.Key; break;
                case "Coffee_OutSide": itemType = eItemType.Coffee_OutSide; break;
                case "Coffee_InSide": itemType = eItemType.Coffee_InSide; break;
            }
            list.Add(itemType);
        }
        return list;
    }

    public static List<Vector2> GetVector2ToAddOnList(string content)
    {
        List<Vector2> list = new List<Vector2>();
        var arrayStr = content.Split('\n');
        foreach (var item in arrayStr)
        {
            var strValue = item.Split(',');
            list.Add(new Vector2(float.Parse(strValue[0]), float.Parse(strValue[1])));
        }
        return list;
    }

    public static List<Vector3> GetVector3ToAddOnList(string content)
    {
        List<Vector3> list = new List<Vector3>();
        var arrayStr = content.Split('\n');
        foreach (var item in arrayStr)
        {
            var strValue = item.Split(',');   
            list.Add(new Vector3(float.Parse(strValue[0]), float.Parse(strValue[1]), float.Parse(strValue[2])));
        }
        return list;
    }

#endif

    public static void DebugLogError(string content)
    {
#if UNITY_EDITOR
        Debug.LogError(content);
#endif
    }

}
