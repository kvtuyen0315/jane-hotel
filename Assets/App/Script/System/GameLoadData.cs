using System.Collections.Generic;
using AFramework;
using UnityEngine;

public class GameLoadData : Singleton<GameLoadData>
{
    public static Dictionary<string, BaseItem> dicItem = new Dictionary<string, BaseItem>();
    public static Dictionary<string, Sprite> dicSpriteChar = new Dictionary<string, Sprite>();
    public static Dictionary<string, BaseCharacter> dicCharacter = new Dictionary<string, BaseCharacter>();
    public static Dictionary<string, MapInfo> dicMapInfo = new Dictionary<string, MapInfo>();
    public static Dictionary<string, Sprite> dicSpriteEmoji = new Dictionary<string, Sprite>();

}
