using AFramework.UI;
using System;
using System.Collections.Generic;
using UnityEngine;

public class MapInfo : BaseUIComp
{
    public Transform posPlayer;
    public Transform posNpc;
    public Transform posKey;
    public Transform posCoffee;

    public PlaceWaitItem[] arrayPlaceWait;

    [HideInInspector] public List<BaseItem> listItems = new List<BaseItem>();

    public ConfigMapRecord configMapRecord { get; private set; }

    public void SetData(ConfigMapRecord configMapRecord)
    {
        this.configMapRecord = configMapRecord;

        for (int i = 0; i < (int)eItemType.Length; i++)
        {
            eItemType itemType = (eItemType)i;
            SpawnItem(itemType);
        }

    }

    private void SpawnItem(eItemType itemType)
    {
        Transform parentItem = null;
        if (itemType == eItemType.Key)
        {
            parentItem = posKey;
        }
        else if (itemType == eItemType.Coffee_InSide || itemType == eItemType.Coffee_OutSide)
        {
            parentItem = posCoffee;
        }
        else
        {
            return;
        }

        int index = (int)itemType;
        ConfigItemRecord configItemRecord = ConfigDataManager.configItem.records[index];
        string nameItem = configItemRecord.name;
        BaseItem baseItem = Instantiate<BaseItem>(GameLoadData.dicItem[nameItem], parentItem);
        baseItem.SetData(configItemRecord);
        baseItem.ShowOrHideTickDone(false);
        listItems.Add(baseItem);
    }

}

[Serializable]
public class PlaceWaitItem
{
    public Transform posNpcWaitItem;
    public bool isHavePerson { get; set; }
}
