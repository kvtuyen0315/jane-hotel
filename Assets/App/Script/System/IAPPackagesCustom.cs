using AFramework.IAP;
using System;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PackageInfoCustom : PackageInfo
{
    public override PackageInfo Clone()
    {
        var newData = new PackageInfoCustom();
        newData.PackageName = PackageName;
        newData.PackageIdentifier = new AFramework.PlatformString(PackageIdentifier);
        newData.Icon = Icon;
        newData.CustomIcon = CustomIcon;
        newData.Type = Type;
        newData.Title = Title;
        newData.Description = Description;
        newData.Price = Price;
        newData.Currency = Currency;
        newData.DisplayPrice = DisplayPrice;
        newData.Rewards = new List<RewardInfo>();
        for (int i = 0; i < Rewards.Count; ++i)
        {
            newData.Rewards.Add(Rewards[i]);
        }
        return newData;
    }
}

[Serializable]
[CreateAssetMenu(menuName = "ScriptableObject/AFramework/IAP/IAPPackagesCustom")]
public class IAPPackagesCustom : IAPPackages
{
    public List<PackageInfoCustom> CustomData;
    public override PackageInfo[] CurrentData => CustomData.ToArray();

#if UNITY_EDITOR
    [ContextMenu("Sync Data")]
    private void SyncData()
    {
    }
#endif

}
