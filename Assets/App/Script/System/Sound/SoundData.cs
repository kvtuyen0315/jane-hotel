﻿using System;
using UnityEngine;

[Serializable] 
public class SoundData {
    public string nameSound;
    public AudioClip clip;

	[Range(0f,1f)]
	public float volume = 0.7f;
	[Range(0.5f,1.5f)]
	public float pitch = 1f;
	public bool isLoop = false;
}
