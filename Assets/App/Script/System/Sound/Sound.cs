﻿using System;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Sound {

	public Sound(SoundData _data) {
		soundData = _data;
	}

	public SoundData soundData;

	private AudioSource source;

	public void SetSoundData(SoundData _data){
		soundData = _data;
		source.clip = soundData.clip;
		source.loop = soundData.isLoop;
		source.volume = soundData.volume;
	}

	public void SetSource(AudioSource _source){
		source = _source;
		source.clip = soundData.clip;
		source.loop = soundData.isLoop;
		source.volume = soundData.volume;
	}

	public void Play(){
		source.DOKill ();
		source.volume = soundData.volume;
		source.pitch = soundData.pitch;
		source.Play ();
	}

	public void Stop(){
		source.Stop ();
	}

	public void StopSmooth(){
		source.DOFade (0f, 0.4f).OnComplete (() => {
			source.Stop ();
		});
	}

	public void Pause(){
		source.Pause ();
	}

	public void Resume(){
		source.UnPause ();
	}
}

[Serializable]
public class MenuSound {

	public MenuSoundIndex soundIndex;
	public AudioClip clip;

	[Range(0f,1f)]
	public float volume = 0.7f;
	[Range(0.5f,1.5f)]
	public float pitch = 1f;
	public bool isLoop;

	private AudioSource source;


	public void SetSource(AudioSource _source){
		source = _source;
		source.clip = clip;
		source.loop = isLoop;
	}

	public void Play(){
		source.volume = volume;
		source.pitch = pitch;
		source.Play ();
	}

	public void Stop()
	{
		source.volume = volume;
		source.pitch = pitch;
		source.Stop();
	}

	public void GetVolume(float value)
	{
		source.volume = value;
	}
}

[Serializable]
public class Music {

	public MusicIndex musicIndex;
	public AudioClip clip;

	[Range(0f,1f)]
	public float volume = 1f;
	[Range(0.5f,1.5f)]
	public float pitch = 1f;

	private AudioSource source;

	public void SetSource(AudioSource _source){
		source = _source;
		source.clip = clip;
		_source.loop = true;
	}

	public void Play(){
		source.volume = volume;
		source.pitch = pitch;
		source.Play ();
	}

	public void Stop(){
		source.Stop ();
	}

    public void Pause()
    {
        source.Pause();
    }

	public void UnPause()
	{
		source.UnPause();
	}

	public void GetVolume(float value)
	{
		source.volume = value;
	}
}