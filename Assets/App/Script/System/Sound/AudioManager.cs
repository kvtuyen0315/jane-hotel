using AFramework;
using UnityEngine;

public class AudioManager : SingletonMono<AudioManager>
{
    public MenuSound[] sounds;

    public Music[] musics;
    private MusicIndex currentMusic = MusicIndex.None;

    void Start()
    {
        int soundLength = sounds.Length;
        for (int i = 0; i < soundLength; i++)
        {

            int cache = i;
            GameObject go = new GameObject("sound_" + i.ToString());
            go.transform.SetParent(this.transform);

            sounds[cache].SetSource(go.AddComponent<AudioSource>());
        }

        int musicLength = musics.Length;
        for (int i = 0; i < musicLength; i++)
        {

            int cache = i;
            GameObject go = new GameObject("music_" + i.ToString());
            go.transform.SetParent(this.transform);

            musics[cache].SetSource(go.AddComponent<AudioSource>());
        }

    }

    public void PlaySound(MenuSoundIndex _index)
    {

        if (!GameData.I.soundStatus) return;

        int soundLength = sounds.Length;
        for (int i = 0; i < soundLength; i++)
        {
            int cache = i;
            if (sounds[cache].soundIndex == _index)
            {
                sounds[cache].Play();
                return;
            }
        }
    }

    public void StopSound(MenuSoundIndex _index)
    {
        if (!GameData.I.soundStatus) return;

        int soundLength = sounds.Length;
        for (int i = 0; i < soundLength; i++)
        {
            int cache = i;
            if (sounds[cache].soundIndex == _index)
            {
                sounds[cache].Stop();
                return;
            }
        }
    }

    public void GetVolumeSound(float value)
    {
        int soundLength = sounds.Length;
        for (int i = 0; i < soundLength; i++)
        {
            int cache = i;
            sounds[cache].volume = value;
            sounds[cache].GetVolume(value);
        }
    }

    public void GetVolumeMusic(float value)
    {
        int musicLength = musics.Length;
        for (int i = 0; i < musicLength; i++)
        {
            int cache = i;
            musics[cache].volume = value;
            musics[cache].GetVolume(value);
        }
    }

    public void PlayMusic(MusicIndex _index)
    {
        if (!GameData.I.musicStatus) return;

        int musicLength = musics.Length;
        for (int i = 0; i < musicLength; i++)
        {
            int cache = i;
            if (currentMusic != _index)
            {
                StopMusic();
                if (musics[cache].musicIndex == _index)
                {
                    musics[cache].Play();
                    currentMusic = _index;
                }
            }
        }
    }

    public void StopMusic()
    {
        int musicLength = musics.Length;

        for (int i = 0; i < musicLength; i++)
        {
            int cache = i;
            if (musics[cache].musicIndex == currentMusic)
            {
                musics[cache].Stop();
                return;
            }
        }

    }

    public void PauseMusic()
    {
        int musicLength = musics.Length;

        for (int i = 0; i < musicLength; i++)
        {
            int cache = i;
            if (musics[cache].musicIndex == currentMusic)
            {
                musics[cache].Pause();
                return;
            }
        }
    }

    public void UnPauseMusic()
    {
        int musicLength = musics.Length;

        for (int i = 0; i < musicLength; i++)
        {
            int cache = i;
            if (musics[cache].musicIndex == currentMusic)
            {
                musics[cache].UnPause();
                return;
            }
        }

    }
}
