using AFramework;
using System;
using UnityEngine;

public class GameData : SingletonMono<GameData>, ISaveData
{
    SaveGameData _data { get; set; }

    public bool DataChanged { get; set; }

    public object GetData()
    {
        return _data;
    }

    public void OnAllDataLoaded() {}

    public void RegisterSaveData()
    {
        SaveGameManager.I.RegisterMandatoryData("SaveGameData", this as AFramework.ISaveData);
    }

    public void SetData(string data)
    {
        if (data == "") //new save game
        {
            _data = new SaveGameData();
        }
        else
        {
            _data = JsonUtility.FromJson<SaveGameData>(data);
        }

        DataChanged= true;
    }

    #region Sound & Music.
    public bool soundStatus
    {
        get => _data.soundStatus;
        set
        {
            _data.soundStatus = value;
            DataChanged= true;
        }
    }

    public bool musicStatus
    {
        get => _data.musicStatus;
        set
        {
            _data.musicStatus = value;
            DataChanged = true;
        }
    }
    #endregion

    #region Story.
    public int indexStory
    {
        get => _data.indexStory;
        set
        {
            _data.indexStory = value;
            DataChanged= true;
        }
    }
    #endregion

    #region Save Data.
    [Serializable]
    public class SaveGameData
    {
        // Sound & Music.
        public bool soundStatus;
        public bool musicStatus;

        // Story.
        public int indexStory;

    }
    #endregion
}
