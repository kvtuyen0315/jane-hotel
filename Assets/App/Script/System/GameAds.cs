﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AFramework.UI;
using AFramework.Ads;
using DG.Tweening;
#if USE_ADMOB
using AdsAdapter = AFramework.Ads.AdmobAdapter;
#elif USE_IRONSOURCE_ADS
using AdsAdapter = AFramework.Ads.IronSourceAdapter;
#elif USE_APPLOVIN_ADS
using AdsAdapter = AFramework.Ads.AppLovinAdapter;
#else
using AdsAdapter = AFramework.Ads.BaseAdsAdapter;
#endif
using AdsConfig = AFramework.Ads.BaseAdapterConfig;

public class GameAdsManager : AFramework.SingletonMono<GameAds> { }
public class GameAds : AFramework.Ads.AdsManager
{
    public const string INTERSTITIAL_LOCATION_START = "start";
    public const string INTERSTITIAL_LOCATION_END = "end";
    public const string INTERSTITIAL_LOCATION_EXIT = "exit";
    public static System.Action UpdateProgressADS = null;

#if PREMIUM
    public override void SetData(string data)
    {
        base.SetData(data);
        mSaveData.RemoveAds = true;
    }

    public override bool IsAdsRemoved()
    {
        return true;
    }
#endif

    protected override void OnRewardAdsViewed(bool result)
    {
        base.OnRewardAdsViewed(result);
        if (result)
        {
            //DailyTaskManager.I.UpdateMissionProgress(3, 1);

            //{
            //    var totalAdsHeroList = ConfigDataManager.fightertypeList[CurrencyType.ADS_02];
            //    var totalAdsViewed = GetRewardAdsViewNumber();
            //    int id = -1;
            //    for (int i = 0; i < totalAdsHeroList.Count; ++i)
            //    {
            //        var config = totalAdsHeroList[i];
            //        if (totalAdsViewed >= config.unlock_price && !GameData.I.IsUnlockedFighter(config.id))
            //        {
            //            id = config.id;
            //            GameData.I.UnlockFighter(id, FighterUnlockReason.REWARD, "totalads");
            //        }                    
            //    }
            //    if (UpdateProgressADS != null) UpdateProgressADS();
            //    if (id >= 0)
            //    {
            //        StartCoroutine(CRClaimCharTotalRewardThread(id));

            //        var shopMenu = AFramework.UI.CanvasManager.GetMenu(GlobalInfo.UIMenuShop, false);
            //        if (shopMenu != null && shopMenu.gameObject.activeSelf) (shopMenu as ShopMenu).UpdateTabHeroes();
            //    }
            //}
        }
    }

    static void OnFullScreenAdShowSuccess()
    {
        //isFullShowing = false;
        //AFramework.UI.CanvasManager.Pop(GlobalInfo.UIPopupLoading);
    }

    protected override void Start()
    {
        base.Start();

        EventOnFullScreenAdsShown += OnFullScreenAdShowSuccess;
    }

#if UNITY_EDITOR || USE_CHEAT
    public void RewardAdsViewed(int value)
    {
        mSaveData.RewardAdsViewed += value;
        DataChanged = true;
    }
#endif
}
