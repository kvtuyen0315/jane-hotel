using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class NpcInfo
{
    public string nameNpc;
    public int npcLevelAI;

    public NpcInfo(string nameNpc, int npcLevelAI)
    {
        this.nameNpc = nameNpc;
        this.npcLevelAI = npcLevelAI;
    }
}

[Serializable]
public class ConfigStoryRecord
{
    public int mapId;
    public List<string> listNamePlayer = new List<string>();
    public List<NpcInfo> listNpcInfo = new List<NpcInfo>();

#if UNITY_EDITOR
    [HideInInspector] public string namePlayer;
    [HideInInspector] public string nameNpc;
    [HideInInspector] public string contentNpcLevelAI;
#endif
}

[CreateAssetMenu(menuName = "Game/ConfigStory")]
public class ConfigStory : BNDataTable<ConfigStoryRecord>
{
#if UNITY_EDITOR
    public override void Sync()
    {
        ReadGoogleSheet.FillData<ConfigStoryRecord>(sheetId, gridId, lst =>
        {
            records.Clear();
            foreach (ConfigStoryRecord item in lst)
            {
                ConfigStoryRecord config = new ConfigStoryRecord();
                config.mapId = item.mapId;

                config.listNamePlayer = GlobalInfo.GetStringToAddOnList(item.namePlayer);

                var listNameNpc = GlobalInfo.GetStringToAddOnList(item.nameNpc);
                var listNpcLevelAI = GlobalInfo.GetIntToAddOnList(item.contentNpcLevelAI);
                for (int i = 0; i < listNameNpc.Count; i++)
                {
                    config.listNpcInfo.Add(new NpcInfo(listNameNpc[i], listNpcLevelAI[i]));
                }

                records.Add(config);
            }

            GameHelper.SetDirty(this);
        });
    }
#endif
}
