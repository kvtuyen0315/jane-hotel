using System;
using UnityEngine;

[Serializable]
public class ConfigCharacterRecord
{
    public string name;
    public int id;
    public string linkPrefab;
    public float moveSpr;
}

[CreateAssetMenu(menuName = "Game/ConfigCharacter")]
public class ConfigCharacter : BNDataTable<ConfigCharacterRecord>
{
#if UNITY_EDITOR
    public override void Sync()
    {
        ReadGoogleSheet.FillData<ConfigCharacterRecord>(sheetId, gridId, lst =>
        {
            records = lst;
            GameHelper.SetDirty(this);
        });
    }
#endif
}
