using System;
using UnityEngine;

[Serializable]
public class ConfigItemRecord
{
    public string name;
    public string linkPrefab;
    public int id;
    public eItemType itemType;
    public bool canDrag;

}

[CreateAssetMenu(menuName = "Game/ConfigItem")]
public class ConfigItem : BNDataTable<ConfigItemRecord>
{
#if UNITY_EDITOR
    public override void Sync()
    {
        ReadGoogleSheet.FillData<ConfigItemRecord>(sheetId, gridId, lst =>
        {
            records = lst;
            GameHelper.SetDirty(this);
        });
    }
#endif
}
