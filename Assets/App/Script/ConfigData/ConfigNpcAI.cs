using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class NpcOderItemInfo
{
    public eItemType itemType;
    public float durationWait;

    public NpcOderItemInfo(eItemType itemType, float durationWait)
    {
        this.itemType = itemType;
        this.durationWait = durationWait;
    }
}

[Serializable]
public class ConfigNpcAIRecord
{
    public int levelAI;
    public List<NpcOderItemInfo> listOderItem = new List<NpcOderItemInfo>();

#if UNITY_EDITOR
    [HideInInspector] public string contentNpcOrderItem;
    [HideInInspector] public string contentNpcDutionWait;
#endif
}

[CreateAssetMenu(menuName = "Game/ConfigNpcAI")]
public class ConfigNpcAI : BNDataTable<ConfigNpcAIRecord>
{
#if UNITY_EDITOR
    public override void Sync()
    {
        ReadGoogleSheet.FillData<ConfigNpcAIRecord>(sheetId, gridId, lst =>
        {
            records.Clear();
            foreach (ConfigNpcAIRecord item in lst)
            {
                ConfigNpcAIRecord config = new ConfigNpcAIRecord();
                config.levelAI = item.levelAI;
                List<eItemType> listItemType = GlobalInfo.GetItemTypeToAddOnList(item.contentNpcOrderItem);
                List<float> listDurationWait = GlobalInfo.GetFloatToAddOnList(item.contentNpcDutionWait);
                for (int i = 0; i < listItemType.Count; i++)
                {
                    config.listOderItem.Add(new NpcOderItemInfo(listItemType[i], listDurationWait[i]));
                }
                records.Add(config);
            }

            GameHelper.SetDirty(this);
        });
    }
#endif
}
