using System;
using UnityEngine;

[Serializable]
public class ConfigMapRecord
{
    public string mapName;
    public string mapId;
    public string linkMapPrefab;
}

[CreateAssetMenu(menuName = "Game/ConfigMap")]
public class ConfigMap : BNDataTable<ConfigMapRecord>
{
#if UNITY_EDITOR
    public override void Sync()
    {
        ReadGoogleSheet.FillData<ConfigMapRecord>(sheetId, gridId, lst =>
        {
            records = lst;
            GameHelper.SetDirty(this);
        });
    }

#endif
}
