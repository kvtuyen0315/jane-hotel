public enum eStateId
{
    None = -1,
    Intro,
    Idle,
    Wait,
    Walk,
    TakeItem,
    TakeOutItem,
    Happy,
    Angry,
}

public enum eStateEmotion
{
    None = -1,
    Love,
    Happy,
    Angry,
    GoAway,
}

public enum eTask
{
    None = -1,
    TakeKey,
    TakeCoffe,
    TakePhone,
}

public enum eWayPointType
{
    None = -1,
    Top,
    Middle,
    Bottom,
}

public enum eCharacterType
{
    None = -1,
    Player,
    Npc,
}

public enum eItemType
{
    None = -1,
    Wait,
    Key,
    Coffee_OutSide,
    Coffee_InSide,
    Length
}