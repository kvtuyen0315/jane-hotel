using AFramework.UI;
using UnityEngine;

public class WayPointToInfo : BaseUIComp
{
    public Transform[] arrayWayPointTo = new Transform[0];
    public eWayPointType pointType = eWayPointType.None;
}
