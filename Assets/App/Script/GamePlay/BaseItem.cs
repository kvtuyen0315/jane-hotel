using AFramework.UI;
using UnityEngine;

public class BaseItem : BaseUIComp
{
    [SerializeField] Collider2D collider2D;
    public SpriteRenderer imgItem;
    [SerializeField] GameObject imgTickDone;

    public Vector3 backupPos { get; private set; }

    public ConfigItemRecord configItemRecord { get; private set; }

    private void Awake()
    {
        backupPos = transform.position;
    }

    public void SetData(ConfigItemRecord configItemRecord)
    {
        this.configItemRecord = configItemRecord;
    }

    public void GetFollowMousePos()
    {
        Vector3 pos = CameraController.I.cameraMain.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector3(pos.x, pos.y, 0f);
    }

    public void ReturnBackOldPos() => transform.position = backupPos;
    public void ShowOrHideTickDone(bool isActive) => imgTickDone.SetActive(isActive);
}
