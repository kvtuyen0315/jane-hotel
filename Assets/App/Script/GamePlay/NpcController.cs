using DG.Tweening;
using UnityEngine;

public class NpcController : BaseCharacter
{
    public CapsuleCollider2D colCapsual;
    public NpcAI npcAI;

    eItemType _orderItemType = eItemType.None;

    [SerializeField] SpriteRenderer imgOderItem;
    [SerializeField] SpriteRenderer imgEmoji;

    [SerializeField] GameObject imgTickDone;

    protected override void Awake()
    {
        base.Awake();

        imgTickDone.SetActive(false);
    }

    public override void Init(Transform posInit)
    {
        base.Init(posInit);

        npcAI.SetData(this);
    }

    public bool orderComplete { get; private set; }
    public void SetOrderComplete(bool status) => orderComplete = status;

    public void OderItemYouWant(eItemType itemType) => _orderItemType = itemType;
    public eItemType GetOrderItem() => _orderItemType;

    public void CompleteTask()
    {
        SetOrderComplete(false);
        OderItemYouWant(eItemType.None);
        SetImgOderItem(null);
        ShowOrHideTickDone(false);
        SetImgEmoji(GameLoadData.dicSpriteEmoji[npcAI.stateEmotion.ToString()]);
        StopTask();
        DOVirtual.DelayedCall(1.5f, () =>
        {
            SetImgEmoji(null);
            CheckNextTask();
        });
    }

    public void ShowOrHideTickDone(bool isActive) => imgTickDone.SetActive(isActive);

    public void StartTask() => npcAI.StartTask();
    public void StopTask() => npcAI.StopTask();
    public void CheckNextTask() => npcAI.CheckNextTask();

    public void SetImgEmoji(Sprite spriteEmoji)
    {
        imgEmoji.sprite = spriteEmoji;        
    }

    public void SetImgOderItem(Sprite spriteOderItem)
    {
        imgOderItem.sprite = spriteOderItem;
    }

    public override void MoveToTarget(SequenceTask sequenceAction)
    {
        sequenceAction.pos = new Vector3(sequenceAction.pos.x, sequenceAction.pos.y, 0f);
        onMoving = true;
        transform.DOKill();
        transform.DOMove(sequenceAction.pos, configCharacterRecord.moveSpr)
            .SetSpeedBased(true)
            .OnComplete(() =>
            {
                sequenceAction.callback?.Invoke();
                onMoving = false;
            });
    }

    protected override void Update()
    {
        base.Update();

        npcAI.DoingTask();
    }

}
