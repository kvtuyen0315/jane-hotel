public class BasicState
{
    public BaseCharacter parent { get; set; }
    public eStateId stateId { get; set; }

    public float runDeltalTime;

    public virtual void LoadData() { }

    public virtual void EnterState() { }

    public virtual void ExitState() { }

    public virtual void CheckNextState() { }

    public virtual void ExcuseState() { }

    public virtual bool CanEnterState() { return true; }

}

public class NullState : BasicState
{
    public NullState()
    {
        stateId = eStateId.None;
    }

}
