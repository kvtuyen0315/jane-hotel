public class IdleState : BasicState
{
    public IdleState(BaseCharacter parent)
    {
        stateId = eStateId.Idle;
        this.parent = parent;
    }

    public override void EnterState()
    {
        
    }
}
