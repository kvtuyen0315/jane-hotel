using AFramework.UI;
using UnityEngine;

public class CameraController : BaseUIComp
{
    public static CameraController I { get; private set; }

    [SerializeField] Transform position;
    [SerializeField] Transform rotation;
    public Camera cameraMain;

    private void Awake()
    {
        I = this;
    }

    public static void ShowOrHideCamera(bool isActive) => I.cameraMain.gameObject.SetActive(isActive);

}
