using AFramework.UI;
using DG.Tweening;
using System;
using UnityEngine;

public class BaseCharacter : BaseUIComp
{    
    public int indexCounter { get; set; }

    [SerializeField] protected SpriteRenderer imgChar;
    [SerializeField] SpriteRenderer imgCharPreviewOnUnity;
    [SerializeField] public eCharacterType characterType = eCharacterType.None;

    public ConfigCharacterRecord configCharacterRecord { get; set; }
    public string nameChar { get => configCharacterRecord.name; }

    #region FSM
    public BasicState nullState { get; set; }
    public IntroState introState { get; set; }
    public IdleState IdleState { get; set; }
    public BasicState currentState { get; set; }

    protected virtual void MakeFSM()
    {
        nullState = new BasicState();
        introState = new IntroState(this);
        IdleState = new IdleState(this);

        currentState = nullState;
    }
    #endregion

    [HideInInspector] public bool isPause = true;

    public bool onMoving { get; protected set; }

    protected virtual void Awake()
    {
        Destroy(imgCharPreviewOnUnity.gameObject);
    }

    public virtual void Init(Transform posInit)
    {
        imgChar.sprite = GameLoadData.dicSpriteChar[nameChar];

        transform.position = posInit.position;

        MakeFSM();

        isPause = false;
    }

    public void PlayAnimation()
    {

    }

    public void ChangeState(BasicState state)
    {
        currentState.ExitState();
        currentState.runDeltalTime = 0f;
        currentState = state;
        currentState.EnterState();
    }

    protected virtual void Update()
    {
        if (isPause) return;

        currentState.runDeltalTime += Time.deltaTime;
        currentState.ExcuseState();
        currentState.CheckNextState();

    }

    public virtual void MoveToTarget(SequenceTask sequenceAction) { }

}