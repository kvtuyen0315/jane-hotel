using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : BaseCharacter
{
    public bool canNoneItemType { get; set; }
    public eItemType currentItemType = eItemType.None;
    public void SetCurrentItemType(eItemType itemType) => currentItemType = itemType;

    [SerializeField] SpriteRenderer imgItemType;

    List<SequenceTask > _listSequenceAction = new List<SequenceTask>();

    public void SetImgOderItem(Sprite spriteOderItem)
    {
        imgItemType.sprite = spriteOderItem;
    }

    public override void MoveToTarget(SequenceTask sequenceAction)
    {
        _listSequenceAction.Add(sequenceAction);

        CheckNextMove();
    }


    private void CheckNextMove()
    {
        if (_listSequenceAction.Count == 0)
        {
            if (canNoneItemType)
            {
                canNoneItemType = false;
                SetCurrentItemType(eItemType.None);
                SetImgOderItem(null);
            }
            return;
        }

        SequenceTask currentAction = _listSequenceAction[0];

        currentAction.pos = new Vector3(currentAction.pos.x, currentAction.pos.y, 0f);
        onMoving = true;
        transform.DOKill();
        transform.DOMove(currentAction.pos, configCharacterRecord.moveSpr)
            .SetSpeedBased(true)
            .OnComplete(() =>
            {
                currentAction.callback?.Invoke();
                onMoving = false;
                _listSequenceAction.RemoveAt(0);
                CheckNextMove();
            });
    }

}
