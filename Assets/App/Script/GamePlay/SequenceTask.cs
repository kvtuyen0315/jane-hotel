using System;
using UnityEngine;

[Serializable]
public class SequenceTask
{
    public Vector3 pos;
    public Action callback;

    public SequenceTask(Vector3 pos, Action callback = null)
    {
        this.pos = pos;
        this.callback = callback;
    }
}
