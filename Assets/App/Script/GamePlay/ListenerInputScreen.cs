using AFramework.UI;
using UnityEngine;
using UnityEngine.EventSystems;

public class ListenerInputScreen : BaseUIComp, /*IDragHandler, IEndDragHandler, */IPointerClickHandler
{
    public static ListenerInputScreen I { get; private set; }

    private void Awake()
    {
        I = this;
    }

    PlayerController player => LevelManager.listCharacterPlayer[0];

    public BaseItem currentItem { get; private set; }

    public bool isDrag { get; private set; }

    private bool CheckHitRay(out RaycastHit2D hit, string nameLayer)
    {
        var ray = CameraController.I.cameraMain.ScreenPointToRay(Input.mousePosition);
        hit = Physics2D.Raycast(ray.origin, ray.direction);
        return hit.transform != null && hit.transform.CompareTag(nameLayer);
    }

    private void TakeItem(BaseItem baseItem, Vector3 posItem)
    {
        currentItem = baseItem;
        currentItem.ShowOrHideTickDone(true);
        SequenceTask sequenceAction = new SequenceTask(posItem, () =>
        {
            currentItem.ShowOrHideTickDone(false);
            player.SetImgOderItem(currentItem.imgItem.sprite);
        });
        player.SetCurrentItemType(currentItem.configItemRecord.itemType);
        player.MoveToTarget(sequenceAction);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (isDrag) return;

        if (CheckHitRay(out RaycastHit2D hit, "CAN_INPUT"))
        {
            if (currentItem)
            {
                BaseItem item = hit.transform.GetComponent<BaseItem>();
                if (item)
                {
                    foreach (NpcController npc in LevelManager.listCharacterNpc)
                    {
                        if (!npc.orderComplete && npc.GetOrderItem() == item.configItemRecord.itemType)
                        {
                            TakeItem(item, hit.point);
                            break;
                        }
                    }
                }
                else
                {
                    NpcController npc = hit.transform.GetComponent<NpcController>();
                    if (!npc || player.currentItemType == eItemType.None) return;

                    if (!npc.orderComplete && npc.GetOrderItem() == player.currentItemType)
                    {
                        npc.SetOrderComplete(true);
                        npc.ShowOrHideTickDone(true);
                        SequenceTask sequenceAction = new SequenceTask(npc.transform.position, () =>
                        {
                            player.canNoneItemType = true;
                            npc.CompleteTask();
                        });
                        player.MoveToTarget(sequenceAction);
                    }
                }
            }
            else
            {   
                BaseItem item = hit.transform.GetComponent<BaseItem>();
                if (item)
                {
                    foreach (NpcController npc in LevelManager.listCharacterNpc)
                    {
                        if (!npc.orderComplete && npc.GetOrderItem() == item.configItemRecord.itemType)
                        {
                            TakeItem(item, hit.point);
                            break;
                        }
                    }

                }
            }
        }
        else if (CheckHitRay(out RaycastHit2D hitMove, "AREA_MOVE")
              && player.currentItemType == eItemType.None
              && !player.onMoving)
        {
            SequenceTask sequenceAction = new SequenceTask(hitMove.point);
            player.MoveToTarget(sequenceAction);
        }
    }

    #region Drag
    public void OnDrag(PointerEventData eventData)
    {
        isDrag = true;

        if (currentItem && currentItem.configItemRecord.canDrag)
        {
            currentItem.GetFollowMousePos();
        }
        else if (CheckHitRay(out RaycastHit2D hit, "CAN_INPUT"))
        {
            currentItem = hit.transform.GetComponent<BaseItem>();

            if (currentItem)
            {
                if (currentItem.configItemRecord.canDrag)
                {
                    currentItem.GetFollowMousePos();
                }
                else
                {
                    currentItem.ShowOrHideTickDone(true);
                }
            }
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        isDrag = false;
        if (currentItem)
        {
            if (CheckHitRay(out RaycastHit2D hit, "CAN_INPUT"))
            {
                foreach (NpcController currentNpc in LevelManager.listCharacterNpc)
                {
                    if (currentNpc.GetOrderItem() == currentItem.configItemRecord.itemType)
                    {
                        GlobalInfo.DebugLogError("character: " + currentNpc.configCharacterRecord.name);
                    }
                }
            }
            currentItem.ReturnBackOldPos();
            currentItem = null;
        }
    } 
    #endregion

}
