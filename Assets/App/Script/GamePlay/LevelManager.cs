using AFramework.UI;
using NSubstitute;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : BaseUIComp
{
    public static LevelManager I { get; private set; }

    [SerializeField] GameObject parentObj;

    public static List<PlayerController> listCharacterPlayer = new List<PlayerController>();
    public static List<NpcController> listCharacterNpc = new List<NpcController>();

    public static Action startLevel { get; set; }
    public static Action endLevel { get; set; }

    public List<WayPointToInfo> listWayPointToInfo = new List<WayPointToInfo>();

    public MapInfo currentMap { get; private set; }

    private void Awake()
    {
        I = this;
    }

    private void OnEnable()
    {
        startLevel += StartLevel;
        endLevel += EndLevel;
    }

    private void OnDisable()
    {
        startLevel -= StartLevel;
        endLevel -= EndLevel;
    }

    public void Init()
    {
        ConfigStoryRecord currentStory = GlobalInfo.currentStory;

        #region Spawn Map.
        ConfigMapRecord configMapRecord = ConfigDataManager.configMap.records[currentStory.mapId];
        currentMap = Instantiate<MapInfo>(GameLoadData.dicMapInfo[configMapRecord.mapName], parentObj.transform);
        currentMap.SetData(configMapRecord);
        currentMap.transform.localPosition = Vector3.zero;
        #endregion

        #region Spawn Character.
        for (int i = 0; i < currentStory.listNamePlayer.Count; i++)
        {
            AddCharacter(ref listCharacterPlayer, i, currentStory.listNamePlayer[i]);
        }

        for (int i = 0; i < currentStory.listNpcInfo.Count; i++)
        {
            AddCharacter(ref listCharacterNpc, i, currentStory.listNpcInfo[i].nameNpc);
        }

        foreach (var item in listCharacterPlayer)
        {
            item.Init(currentMap.posPlayer);
        }

        foreach (var item in listCharacterNpc)
        {
            item.Init(currentMap.posNpc);
        }
        #endregion

        startLevel?.Invoke();
    }

    private void AddCharacter(ref List<PlayerController> listCharacter, int indexCounter, string nameCharacter)
    {
        BaseCharacter character = Instantiate<BaseCharacter>(GameLoadData.dicCharacter[nameCharacter], parentObj.transform);
        character.configCharacterRecord = ConfigDataManager.configCharacter.records.Find(s => s.name == nameCharacter);
        character.indexCounter = indexCounter;
        listCharacter.Add(character as PlayerController);

        Sprite[] arraySpriteChar = Resources.LoadAll<Sprite>("ImageData/Character/" + nameCharacter);
        foreach (Sprite spriteChar in arraySpriteChar)
        {
            if (!GameLoadData.dicSpriteChar.ContainsKey(nameCharacter))
            {
                GameLoadData.dicSpriteChar.Add(nameCharacter, spriteChar);
            }
        }
    }

    private void AddCharacter(ref List<NpcController> listCharacter, int indexCounter, string nameCharacter)
    {
        BaseCharacter character = Instantiate<BaseCharacter>(GameLoadData.dicCharacter[nameCharacter], parentObj.transform);
        character.configCharacterRecord = ConfigDataManager.configCharacter.records.Find(s => s.name == nameCharacter);
        character.indexCounter = indexCounter;
        listCharacter.Add(character as NpcController);

        Sprite[] arraySpriteChar = Resources.LoadAll<Sprite>("ImageData/Character/" + nameCharacter);
        foreach (Sprite spriteChar in arraySpriteChar)
        {
            if (!GameLoadData.dicSpriteChar.ContainsKey(nameCharacter))
            {
                GameLoadData.dicSpriteChar.Add(nameCharacter, spriteChar);
            }
        }
    }

    public static void HideOrShow(bool isActive)
    {
        I.parentObj.SetActive(isActive);
        if (!isActive && I.currentMap)
        {
            CameraController.ShowOrHideCamera(false);
            Destroy(I.currentMap.gameObject);

            foreach (var item in listCharacterPlayer)
            {
                Destroy(item.gameObject);
            }
            listCharacterPlayer.Clear();

            foreach (var item in listCharacterNpc)
            {
                Destroy(item.gameObject);
            }
            listCharacterNpc.Clear();

            GameLoadData.dicSpriteChar.Clear();
        }
    }

    private void StartLevel()
    {
        if (crStartLevel != null)
        {
            StopCoroutine(crStartLevel);
        }

        crStartLevel = CrStartLevel();
        StartCoroutine(crStartLevel);
    }
    IEnumerator crStartLevel { get; set; }
    private IEnumerator CrStartLevel()
    {
        yield return null;

        foreach (var item in listCharacterNpc)
        {
            item.StartTask();
        }
    }

    private void EndLevel()
    {
        if (crEndLevel != null)
        {
            StopCoroutine(crEndLevel);
        }

        crEndLevel = CrEndLevel();
        StartCoroutine(crEndLevel);
    }
    IEnumerator crEndLevel { get; set; }
    private IEnumerator CrEndLevel()
    {
        yield return null;

        foreach (var item in listCharacterNpc)
        {
            item.StopTask();
        }

        BlockPopup.Show(null);
        LevelMapMenu.Hide();
        MainMenu.Show(null);
        BlockPopup.Hide();
    }

}
