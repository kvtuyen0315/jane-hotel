﻿using AFramework.UI;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class NpcAI : BaseUIComp
{
    public NpcController npc { get; private set; }
    public ConfigNpcAIRecord configNpcAIRecord { get; private set; }

    public eItemType getTaskType { get => _currentOderItemInfo.itemType; }
    public int taskCounter { get; private set; }
    List<NpcOderItemInfo> _listOderItemInfo = new List<NpcOderItemInfo>();
    NpcOderItemInfo _currentOderItemInfo { get; set; }

    bool _canCounterTime { get; set; }
    float _durationCountDown { get; set; }
    float _targetTimeToHappy { get; set; }
    float _targetTimeToAngry { get; set; }

    public eStateEmotion stateEmotion { get; private set; }

    bool _isInit { get; set; }

    PlayerController player => LevelManager.listCharacterPlayer[0];

    public void SetData(NpcController npcController)
    {
        this.npc = npcController;
        int npclevelAI = GlobalInfo.currentStory.listNpcInfo[npcController.indexCounter].npcLevelAI;
        //List<ConfigNpcAIRecord> listConfigNpcAI = ConfigDataManager.configNpcAI.records.Where(s => s.contentNpcOrderItem)

        configNpcAIRecord = ConfigDataManager.configNpcAI.records[npclevelAI];
        _listOderItemInfo.AddRange(configNpcAIRecord.listOderItem);

        _isInit = true;
    }

    public void StartTask()
    {
        _canCounterTime = true;
        _currentOderItemInfo = _listOderItemInfo[taskCounter];

        PlaceWaitItem placeWait = null;
        Vector3 targetPos = Vector3.zero;
        if (_isInit)
        {
            placeWait = LevelManager.I.currentMap.arrayPlaceWait[(int)eItemType.Wait];
            targetPos = placeWait.posNpcWaitItem.position;
        }
        else
        {
            placeWait = LevelManager.I.currentMap.arrayPlaceWait[(int)getTaskType];
            if (placeWait.isHavePerson)
            {
                _isInit = true;
                StartTask();
                return;
            }
            else
            {
                targetPos = placeWait.posNpcWaitItem.position;
            }
        }

        SequenceTask sequenceAction = new SequenceTask(targetPos, () =>
        {
            if (_isInit)
            {
                _isInit = false;
                StartTask();
            }
            else
            {
                eItemType itemType = _currentOderItemInfo.itemType;
                npc.OderItemYouWant(itemType);
                _durationCountDown = _currentOderItemInfo.durationWait;
                _targetTimeToHappy = _currentOderItemInfo.durationWait * 0.75f;
                _targetTimeToAngry = _currentOderItemInfo.durationWait * 0.25f;
                int indexItem = (int)itemType;
                npc.SetImgOderItem(LevelManager.I.currentMap.listItems[indexItem].imgItem.sprite);
            }
        });
        npc.MoveToTarget(sequenceAction);
    }

    public void StopTask()
    {
        _canCounterTime = false;
        
    }

    public void CheckNextTask()
    {
        if (stateEmotion == eStateEmotion.GoAway)
        {
            taskCounter = _listOderItemInfo.Count - 1;
            // Cho thì cho đi luok.
            player.SetCurrentItemType(eItemType.None);
            player.SetImgOderItem(null);
            npc.SetOrderComplete(false);
            npc.OderItemYouWant(eItemType.None);
            npc.SetImgOderItem(null);
        }
        else
        {
            taskCounter++;
            // Cho 1 biến counter Angry-- , khi = 0 thì cho đi luok.
        }

        if (taskCounter >= _listOderItemInfo.Count)
        {
            StopTask();
        }
        else
        {
            StartTask();
        }
    }

    public void MakeStateEmotion(eStateEmotion stateEmotion)
    {
        this.stateEmotion = stateEmotion;
    }

    public void DoingTask()
    {
        if (_canCounterTime && _durationCountDown > 0)
        {
            _durationCountDown -= Time.deltaTime;
            eStateEmotion emotion = eStateEmotion.Happy;

            if (_durationCountDown <= 0)
            {
                emotion = eStateEmotion.GoAway;
            }
            else if (_durationCountDown < _targetTimeToAngry)
            {
                emotion = eStateEmotion.Angry;
            }
            else if (_durationCountDown < _targetTimeToHappy)
            {
                emotion = eStateEmotion.Happy;
            }
            else
            {
                emotion = eStateEmotion.Love;
            }

            MakeStateEmotion(emotion);

            if (_durationCountDown <= 0)
            {
                CheckNextTask();
            }
        }
    }
}
