using AFramework.UI;
using System;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : BaseUIMenu
{
    public static MainMenu I { get; private set; }
    public Action callback { get; set; }

    public static void Show(object[] initParams)
    {
        if (!CanvasManager.IsSpecificUIShown(GlobalInfo.UIMainMenu))
        {
            CanvasManager.Push(GlobalInfo.UIMainMenu, initParams);
        }
    }

    public static void Hide()
    {
        I.Pop();
        BlockPopup.Hide();
        I.callback?.Invoke();
    }

    [SerializeField] Button buttonStartGame;

    private void Awake()
    {
        I = this;

        buttonStartGame.onClick.AddListener(() =>
        {
            AudioManager.I.PlaySound(MenuSoundIndex.CLICK);

            GlobalInfo.currentStory = ConfigDataManager.configStory.records[GameData.I.indexStory];

            LevelManager.HideOrShow(true);
            CameraController.ShowOrHideCamera(true);
            BlockPopup.Show(null);
            LevelMapMenu.Show(null);
            Hide();
        });
    }

    public override void Init(object[] initParams)
    {
        callback = initParams != null ? (Action)initParams[0] : null;

    }
}
