using AFramework.UI;
using System;
using UnityEngine;
using UnityEngine.UI;

public class LevelMapMenu : BaseUIMenu
{
    public static LevelMapMenu I { get; private set; }
    public Action callback { get; set; }

    [SerializeField] Button buttonBackToMain;

    public static void Show(object[] initParams)
    {
        if (!CanvasManager.IsSpecificUIShown(GlobalInfo.UILevelMapMenu))
        {
            CanvasManager.Push(GlobalInfo.UILevelMapMenu, initParams);
        }
    }

    public static void Hide()
    {
        I.Pop();
        BlockPopup.Hide();
        LevelManager.HideOrShow(false);
        I.callback?.Invoke();
    }

    private void Awake()
    {
        I = this;

        buttonBackToMain.onClick.AddListener(() =>
        {
            AudioManager.I.PlaySound(MenuSoundIndex.BACK);

            MainMenu.Show(null);
            BlockPopup.Show(null);
            Hide();
        });
    }

    public override void Init(object[] initParams)
    {
        callback = initParams != null ? (Action)initParams[0] : null;

        LevelManager.I.Init();
    }
}
