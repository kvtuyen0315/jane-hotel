using AFramework.UI;
using System;

public class BlockPopup : BaseUIMenu
{
    public static BlockPopup I { get; private set; }
    public Action callback { get; set; }

    public static void Show(object[] initParams)
    {
        if (!CanvasManager.IsSpecificUIShown(GlobalInfo.UIBlockPopup))
        {
            CanvasManager.Push(GlobalInfo.UIBlockPopup, initParams);
        }
    }

    public static void Hide()
    {
        I.Pop();
        I.callback?.Invoke();
    }

    private void Awake()
    {
        I = this;
    }

    public override void Init(object[] initParams)
    {
        callback = initParams != null ? (Action)initParams[0] : null;
    }

}
