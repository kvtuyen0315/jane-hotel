using AFramework.UI;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FlashScreenPopup : BaseUIMenu
{   
    public override void Init(object[] initParams)
    {
        base.Init(initParams);

        StartCoroutine(LoadingGame());
    }

    private IEnumerator LoadingGame()
    {
        AsyncOperation loadScene = SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive);
        yield return new WaitUntil(() => loadScene.isDone);
        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(1));

        yield return null;
        LevelManager.HideOrShow(false);
        CameraController.ShowOrHideCamera(false);
        CanvasManager.PopSelf(this, true);
    }

}
